'use strict';

const SwaggerHapi = require('swagger-hapi');
const Hapi = require('hapi');
const app = new Hapi.Server();

module.exports = app; // for testing

const config = {
    appRoot: __dirname // required config
};

SwaggerHapi.create(config, (err, swaggerHapi) => {
    if (err) {
        throw err;
    }

    const port = process.env.PORT || 10010;
    app.connection({
        port: port
    });

    app.address = function () {
        return {
            port: port
        };
    };

    app.register(swaggerHapi.plugin, (err) => {
        if (err) {
            return console.error('Failed to load plugin:', err);
        }
        app.start(() => {
            if (swaggerHapi.runner.swagger.paths['/hello']) {
                console.log('try this:\ncurl http://127.0.0.1:' + port + '/hello?name=Scott');
            }
        });
    });
});
