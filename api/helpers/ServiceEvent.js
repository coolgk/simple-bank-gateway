'use strict';

// const AppEvent = require('./AppEvent');
// const appEvent = new AppEvent();
// const PubSub = require(`@google-cloud/pubsub`);

class SerivceEvent {

    constructor (options) {
        this.appEvent = options.appEvent;
        this.publisher = options.serivceEventPublisher;
        this.subscriber = options.serviceEventSubscriber;
    }

    /**
     * publish an event
     *
     * @param {string} event - an event
     * @param {*} data - event payload
     * @returns
     * @memberof SerivceEvent
     */
    publish (event, data) {
        return this.serviceEvent.publish(event, data);
    }

    /**
     * subscribe to an event
     * @param {string} event - an event
     */
    subscribe (event) {
        return this.serviceEvent.subscribe(
            event,
            // publish node event when response event is received
            (message) => this.appEvent.emit(event, message)
        );
    }

    /**
     * Listen to event responses, stop listening and return the result if the callback returns true
     *
     * @param {string} event - an event
     * @param {function} callback - (message) => boolean, a filter function which takes a response message as parameter and returns a boolean value to indicate if the message should be returned
     * @returns {object} - a published message
     * @memberof SerivceEvent
     */
    filter (event, callback) {
        return new Promise((resolve) => {
            this.appEvent.once(
                event,
                (message) => resolve(
                    callback(message) ? message : this.filter(event, callback)
                )
            );
        });
    }

}

module.exports = SerivceEvent;
