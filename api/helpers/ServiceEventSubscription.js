'use strict';

class ServiceEventSubscription {
    constructor (options) {
        this.event = options.event;
        this.appEvent = options.appEvent;
    }

    filter (callback) {
        const message = await this.getMessage()
        callback(message) ? message : this.filter(callback)

        return new Promise((resolve) => {
            this.appEvent.once(
                this.event,
                (message) => resolve(
                    callback(message) ? message : this.filter(callback)
                )
            );
        });
    }
}

module.exports = ServiceEventSubscription;
