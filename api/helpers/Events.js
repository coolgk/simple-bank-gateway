'use strict';

module.exports = {
    ACCOUNT_FETCH_SUMMARY: 'ACCOUNT_FETCH_SUMMARY',
    ACCOUNT_SUMMARY_READY: 'ACCOUNT_SUMMARY_READY',
    TRANSACTION_FETCH_TRANSACTIONS: 'TRANSACTION_FETCH_TRANSACTIONS',
    TRANSACTION_TRANSACTIONS_READY: 'TRANSACTION_TRANSACTIONS_READY'
};
