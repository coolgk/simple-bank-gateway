'use strict';

class ServiceEventSubscriber {

    constructor (options) {
        this.pubsub = options.pubsub;
    }

    subscribe (event) {
        return new Promise((resolve, reject) => {
            return this.pubsub
                .subscription(event)
                .on('message', (message) => resolve(message) && message.ack())
                .on('error', (error) => reject(error));
        });
    }

}

module.exports = ServiceEventSubscriber;
