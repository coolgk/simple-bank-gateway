'use strict';

const AppEvent = require('./AppEvent');
const appEvent = new AppEvent();

const ServiceEvent = require('./ServiceEvent');
const serviceEvent = new ServiceEvent({});

class Controller {
    /**
     * @param {*} options
     */
    constructor (options = {}) {
        this.appEvent = options.appEvent || appEvent;
        this.serviceEvent = options.serviceEvent || serviceEvent;
    }

    publish (event, data) {
        return this.serviceEvent.publish(event, data);
    }

    subscribe (event) {
        this.serviceEvent.subscribe(
            event,
            // publish node event when response event is received
            (message) => this.appEvent.emit(event, message)
        );
    }

    filter (event, callback) {
        return new Promise((resolve) => {
            this.appEvent.once(
                event,
                (message) => resolve(
                    callback(message) ? message : this.filter(event, callback)
                )
            );
        });
    }

    // getResponse (event, message) {
    //     return new Promise((resolve) => {
    //         // consume (response) event
    //         this.appEvent.once(event, (responseMessage, requestMessage) => {
    //             if (requestMessage.id === message.id) {
    //                 resolve(responseMessage);
    //             } else {
    //                 resolve(this.getResponse(event, message));
    //             }
    //         });
    //     });
    // }

    /**
     * subscribe to (response) events published by services

    subscribe () {
        return this.serviceEvent.subscribe(
            this.subscribeTo,
            // publish node event when response event is received
            (message) => this.appEvent.emit(`${this.subscribeTo}_${message.id}`, message)
        );
    }
*/
    /**
     * publish events (to which services can subscribe)
     *
     * @param {*} data

    async publish (data) {
        const message = await this.serviceEvent.publish(this.publishTo, data);
        return new Promise((resolve) => {
            // consume (response) event
            this.appEvent.once(`${this.subscribeTo}_${message.id}`, (message) => {
                resolve(message);
            });
        });
    } */
}

module.exports = Controller;
