'use strict';

class ServiceEventPublisher {

    constructor (options) {
        this.pubsub = options.pubsub;
    }

    publish (event, data) {
        return this.pubsub
            .topic(event)
            .publisher()
            .publish(
                Buffer.from(
                    JSON.stringify(data)
                )
            );
    }
}

module.exports = ServiceEventPublisher;
