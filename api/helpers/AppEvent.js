'use strict';

const Events = require('events');

class AppEvent extends Events {
    constructor (...params) {
        super(...params);
        this.setMaxListeners(Infinity);
    }
}

module.exports = AppEvent;
