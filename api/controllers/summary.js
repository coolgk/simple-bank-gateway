'use strict';

const Events = require('../helpers/Events');
const ServiceEvent = require('../helpers/ServiceEvent');

const summaryEvent = new ServiceEvent();
summaryEvent.subscribe(Events.ACCOUNT_SUMMARY_READY);

module.exports = {
    summary: async (request, response) => {
        const accountId = request.swagger.params.accountid.value;

        const [accountSummary] = await Promise.all([
            summaryEvent.filter(Events.ACCOUNT_SUMMARY_READY, (message) => message.accountId === accountId),
            summaryEvent.publish(Events.ACCOUNT_FETCH_SUMMARY, accountId)
        ]);

        response.json(accountSummary);
    }
};
